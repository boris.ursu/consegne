# Consegne AS 2020 - 2021

Qui proporremo i testi delle consegne che poi verranno riportati come [milestones](https://gitlab.com/groups/zuclassroom2021/-/milestones) di gruppo con le opportuna *label* ad indicare le classi cui sono rivolte. Ogni consegna contiene inoltre i criteri di valutazione.