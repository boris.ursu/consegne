## cronometro o timer o gioco

[A] Si realizzi un'applicazione Flutter per un cronometro ovvero un timer a secondi partendo dall'applicazione basic:  
- uno stream funge da *ticker*
- il precedente stream può essere *mappato* su di un altro stream per otenere i secondi (con bonus valutazione).

[B] Si realizzi un gioco in cui un quadrato cambia colore in un range prefissato di 5 colori (a piacere), ad ogni cambiamento di coloro che avviene in un tempo random fra i 2 ed i 4 secondi il giocatore deve fare click su uno dei cinque bottoni, posizionati opportunamente, dell'analogo colore.

## Nota

[B] è chiaramente più difficile di [A].

## Valutazione

Contribuiscono alla valutazione: la pulizia del codice, la documentazione e le scelte operate. Il progetto deve essere consegnato sulla cartella `cronometro` ovvero `timer` ovvero `gioco` nel progetto (repository) `TPSIT`, la documentazione va nel file `README.md`. Le valutazioni eccellenti (8, 9, 10) sono riservate a coloro che daranno un consistente apporto personale e creativo con attenzione all'usabilità dell'applicazione.  
